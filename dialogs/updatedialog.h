#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QTableWidgetItem>

#include "../config.h"
#include "../util/types.h"
#include "../helper/alpmhelper.h"
#include "../helper/aurhelper.h"

class UpdateDialog : public QDialog
{
    Q_OBJECT
public:
    UpdateDialog(Config *config, QList<pkg_display_t> list, QDialog *parent = 0);
public slots:
    void updateList(QList<pkg_display_t> list);
private:
    ALPMHelper alpm;
    AURHelper aur;
    QTableWidget *update_list;
};

#endif // UPDATEDIALOG_H
