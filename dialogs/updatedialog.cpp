#include "updatedialog.h"

UpdateDialog::UpdateDialog(Config *config, QList<pkg_display_t> list, QDialog *parent)
    :QDialog(parent)
{
    QList<pkg_display_t> repo_packages, aur_packages, updates;

    foreach (pkg_display_t pkg, list) {
        if(pkg.repo == "AUR") aur_packages.append(pkg);
        else repo_packages.append(pkg);
    }

    updates.append(alpm.check_updates(config,repo_packages));
    aur.check_updates(aur_packages);
    connect(&aur,SIGNAL(updatesavilablefor(QList<pkg_display_t>)),this,SLOT(updateList(QList<pkg_display_t>)));

    QVBoxLayout *layout = new QVBoxLayout;
    QStringList labels;
    labels << "" << "Name" << "Old Version" << "New Version";
    update_list = new QTableWidget;
    update_list->setColumnCount(4);
    update_list->setRowCount(updates.size());
    update_list->setHorizontalHeaderLabels(labels);
    update_list->setColumnWidth(0,36);
    update_list->setColumnWidth(1,250);
    int i = 0;

    foreach(pkg_display_t pkg, updates){
        update_list->setRowHeight(i,36);
        update_list->setItem(i,1,new QTableWidgetItem(pkg.name));
        update_list->setItem(i,2,new QTableWidgetItem(pkg.version));
        update_list->setItem(i,3,new QTableWidgetItem(pkg.new_version));
        i++;
    }

    layout->addWidget(update_list);

    setMinimumSize(537, 500);
    setLayout(layout);
}

void UpdateDialog::updateList(QList<pkg_display_t> list)
{
    int i = update_list->rowCount();
    update_list->setRowCount(i+list.size());

    foreach(pkg_display_t pkg, list){
        update_list->setRowHeight(i,36);
        update_list->setItem(i,1,new QTableWidgetItem(pkg.name));
        update_list->setItem(i,2,new QTableWidgetItem(pkg.version));
        update_list->setItem(i,3,new QTableWidgetItem(pkg.new_version));
        i++;
    }
}
