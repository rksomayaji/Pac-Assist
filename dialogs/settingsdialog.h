#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QWidget>
#include <QDir>
#include <QLabel>
#include <QLineEdit>
#include <QTabWidget>
#include <QListWidget>
#include <QTextStream>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QGroupBox>

#include "../config.h"

class LocalSettings : public QWidget
{
    Q_OBJECT
public:
    explicit LocalSettings(QWidget *parent = 0);
};

class RepositoriesTab : public QWidget
{
    Q_OBJECT
public:
    explicit RepositoriesTab(QList<repo_t> repos, QWidget *parent = 0);
};

class SettingsTab : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsTab(Config config, QWidget *parent = 0);
};

class PackagesTab : public QWidget
{
    Q_OBJECT
public:
    explicit PackagesTab(Config config, QWidget *parent = 0);
};

class SettingsDialog : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsDialog(Config config, QWidget *parent = 0);
    ~SettingsDialog();

private:
    QVBoxLayout *main_layout;
    QTabWidget *settings_tabs;
    bool settings_changed;
};

#endif //SETTINGSDIALOG_H
