#include "settingsdialog.h"

QLayout *getSettingsLayout(QString title, QString value)
{
    QLabel *title_label = new QLabel(title);
    QLineEdit *path = new QLineEdit;
    path->setText(value);
    path->setReadOnly(true);
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(title_label);
    layout->addWidget(path);
    return layout;
}

QLayout *getPackagesLayout(QString title, QStringList pkgs)
{
    QLabel *title_label = new QLabel(title);
    QListWidget *pkgs_list = new QListWidget;
    pkgs_list->addItems(pkgs);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(title_label);
    layout->addWidget(pkgs_list);
    return layout;
}

SettingsTab::SettingsTab(Config config, QWidget *parent)
    : QWidget(parent)
{
    QGroupBox *pacman = new QGroupBox("Pacman Settings");
    QVBoxLayout *pacman_layout = new QVBoxLayout;
    pacman_layout->addLayout(getSettingsLayout("Config File",config.getConfigFilePath()));
    pacman_layout->addLayout(getSettingsLayout("Root",config.getRootDir()));
    pacman_layout->addLayout(getSettingsLayout("Database Path",config.getdbPath()));
    pacman_layout->addLayout(getSettingsLayout("Cache Directory",config.getCacheDir()));
    pacman_layout->addLayout(getSettingsLayout("GPG Directory",config.getGPGDir()));
    pacman_layout->addLayout(getSettingsLayout("Log File",config.getLogFilePath()));
    pacman_layout->addLayout(getSettingsLayout("Pacman Hook Directory",config.getHookDir()));
    pacman->setLayout(pacman_layout);

    QVBoxLayout *main_layout = new QVBoxLayout;
    main_layout->addWidget(pacman);
    setLayout(main_layout);
}

PackagesTab::PackagesTab(Config config, QWidget *parent)
    : QWidget(parent)
{
    QGroupBox *packages = new QGroupBox("Packages");
    QVBoxLayout *pkg_layout = new QVBoxLayout;
    pkg_layout->addLayout(getPackagesLayout("Hold Packages", config.getHoldPkgList()));
    pkg_layout->addLayout(getPackagesLayout("Ignore Packages", config.getIgnorePkgList()));
    pkg_layout->addLayout(getPackagesLayout("Ignore Groups", config.getIgnoreGroupList()));
    pkg_layout->addLayout(getPackagesLayout("Don't Upgrade Files", config.getNoUpgradePkgList()));
    pkg_layout->addLayout(getPackagesLayout("Don't Replace These Files", config.getNoExtractFileList()));
    packages->setLayout(pkg_layout);

    QVBoxLayout *main_layout = new QVBoxLayout;
    main_layout->addWidget(packages);
    setLayout(main_layout);
}

RepositoriesTab::RepositoriesTab(QList<repo_t> repos, QWidget *parent)
    :QWidget(parent)
{
    QStringList active, inactive;
    foreach (repo_t r, repos) {
        if(r.isActive) active.append(r.name);
        else inactive.append(r.name);
    }
    QListWidget *active_repo = new QListWidget;
    active_repo->addItems(active);
    QListWidget *inactive_repo = new QListWidget;
    inactive_repo->addItems(inactive);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new QLabel("Active Repositories"));
    layout->addWidget(active_repo);
    layout->addWidget(new QLabel("Inactive Repositories"));
    layout->addWidget(inactive_repo);
    setLayout(layout);
}

LocalSettings::LocalSettings(QWidget *parent)
    :QWidget(parent)
{
    QString path = QDir::homePath() + "/.local/share/Pac-Assist/conf/settings.json";
    QFile local_setting(path);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setAlignment(Qt::AlignTop);
    if(!local_setting.exists()){
        QCheckBox *repo_update = new QCheckBox("Update repositories on start up");
        QCheckBox *check_update = new QCheckBox("Check for updates on start up");
        check_update->setChecked(true);
        QPushButton *save_settings = new QPushButton("Save");
        save_settings->setEnabled(false);
        layout->addWidget(repo_update);
        layout->addWidget(check_update);
        layout->addWidget(save_settings,Qt::AlignBottom);
    }
    setLayout(layout);
}

SettingsDialog::SettingsDialog(Config config, QWidget *parent)
    : QWidget(parent)
{
    settings_tabs = new QTabWidget;
    settings_tabs->addTab(new SettingsTab(config), tr("Settings"));
    settings_tabs->addTab(new RepositoriesTab(config.getConfigRepositories()), tr("Repositories"));
    settings_tabs->addTab(new PackagesTab(config),tr("Packages"));
    settings_tabs->addTab(new LocalSettings(),tr("Application Settings"));

    main_layout = new QVBoxLayout;
    main_layout->addWidget(settings_tabs);
    setLayout(main_layout);
    setGeometry(100,100,500,150);
}

SettingsDialog::~SettingsDialog()
{

}
