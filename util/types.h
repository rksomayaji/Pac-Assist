#ifndef TYPES_H
#define TYPES_H

#include <QString>
#include <QStringList>

#include <git2.h>
#include <alpm.h>
#include <alpm_list.h>

typedef struct _pkg_display_t{
    QString name;
    QString desc;
    QString version;
    QString new_version;
    QString repo;
    bool isInstalled;
    bool updateAvailable;
}pkg_display_t;

typedef struct _aurpkg_t{
    QString desc;
    QString maintainer;
    QString name;
    QString version;
    QString old;
    QString pkgbase;
    QString urlpath;
    QString url;

    QStringList checkdepends;
    QStringList conflicts;
    QStringList depends;
    QStringList groups;
    QStringList keywords;
    QStringList licenses;
    QStringList makedepends;
    QStringList optdepends;
    QStringList provides;
    QStringList replaces;

    unsigned int id;
    unsigned int pkgbase_id;
    unsigned int votes;

    bool outofdate;
    bool update;

    time_t firstsubmit;
    time_t lastmod;

    double popularity;
}aurpkg_t;

typedef struct _version_t{
    QString epoch;
    QString version;
    QString release;
}version_t;

typedef struct progress_data {
  git_transfer_progress fetch_progress;
  size_t completed_steps;
  size_t total_steps;
  const char *path;
} progress_data;

typedef struct _repo_t{
    QString name;
    bool isActive = false;
    QString include_serverlist;
    QStringList server;
    int siglevel;
    int siglevel_mask;
}repo_t;

typedef struct _config_t{
    QString arch;
    QString configfile;
    QString dbpath;
    QString rootdir;
    QString cachedir;
    QString gpgdir;
    QString logfile;
    QString hookdir;

    alpm_handle_t *handle;
    QList<repo_t> repos;

    QStringList holdpkg;
    QStringList ignorepkg;
    QStringList noupgrade;
    QStringList noextract;
    QStringList ignoregrp;

    int siglevel;
    int localfilesiglevel;
    int remotefilesiglevel;
}config_t;


#endif //TYPES_H
