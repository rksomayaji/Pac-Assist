#include "common.h"

Common::Common()
{

}
version_t parse_version(QString version)
{
    version_t ver;

    if(version.contains(":"))
    {
        QString pro, dis;
        pro = version.split(":").at(0);
        dis = version.split(":").at(1);

        if(pro == "") ver.epoch = "0";
        else ver.epoch = pro;

        ver.version = dis.split("-").at(0);
        ver.release = dis.split("-").at(1);
    }
    else{
        ver.epoch = "0";
        ver.version = version.split("-").at(0);
        ver.release = version.split("-").at(1);
    }
    return ver;
}

bool Common::compare_version(QString old_version, QString new_version)
{
    version_t oldv = parse_version(old_version);
    version_t newv = parse_version(new_version);

    if(oldv.epoch.compare(newv.epoch) != 0 ) return true;
    else{
        if(oldv.version.compare(newv.version) != 0 ) return true;
        else{
            if(oldv.release.compare(newv.release) != 0 ) return true;
            else return false;
        }
    }
}
