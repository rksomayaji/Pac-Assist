#ifndef COMMON_H
#define COMMON_H

#include <QTextStream>

#include "types.h"

class Common
{
public:
    Common();
    bool compare_version(QString old_version, QString new_version);
};

#endif // COMMON_H
