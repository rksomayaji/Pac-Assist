#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QToolBox>
#include <QLineEdit>
#include <QGroupBox>
#include <QCheckBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextStream>
#include <QTableWidget>
#include <QTableWidgetItem>

#include <alpm.h>
#include <alpm_list.h>

#include "helper/alpmhelper.h"
#include "helper/aurhelper.h"
#include "util/types.h"
#include "util/common.h"
#include "dialogs/settingsdialog.h"
#include "dialogs/updatedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void resizeEvent(QResizeEvent *event);
    ~MainWindow();
public slots:
    void refresh_installed_package_list();
    void search_packages();
    void display_aur_package_list(QList<aurpkg_t> list);
    void display_repo_package_list(QList<pkg_display_t> list);
    void display_packages_list(QList<aurpkg_t> list);
private:
    Ui::MainWindow *ui;

    QVBoxLayout *main_layout;
    QHBoxLayout *search_layout;

    QPushButton *search_button;
    QPushButton *refresh_button;
    QPushButton *update_button;

    QLineEdit *search_bar;

    QTableWidget *package_list_widget;

    QAction *quit_action;
    QAction *aur_option;
    QAction *repo_option;

    QAction *settings_action;
    void create_display();
    void set_connections();
    void set_column_width(int widget_width);
    int get_flag();
    void display_package_list();
    bool is_installed(QString pkgname);
    bool update_available(QString pkgname, QString new_version);

    Config config;
    ALPMHelper alpmhelper;
    AURHelper aurhelper;
    SettingsDialog *settings;
    UpdateDialog *updates;
    QList<pkg_display_t> total_list;
    Common *common;
};

#endif // MAINWINDOW_H
