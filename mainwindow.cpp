﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

int MainWindow::get_flag()
{
    if(aur_option->isChecked() && !repo_option->isChecked()) return 0;
    else if(!aur_option->isChecked() && repo_option->isChecked()) return 1;
    else if(aur_option->isChecked() && repo_option->isChecked()) return 2;
    else return -1;
}

void MainWindow::set_column_width(int widget_width)
{
    package_list_widget->setColumnWidth(0,50);
    package_list_widget->setColumnWidth(1,widget_width-388);
    package_list_widget->setColumnWidth(2,176);
    package_list_widget->setColumnWidth(3,100);
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    set_column_width(event->size().width()-22);
}

bool MainWindow::is_installed(QString pkgname)
{
    alpm_db_t *localdb = alpm_get_localdb(config.getConfigHandle());
    if(alpm_db_get_pkg(localdb,pkgname.toStdString().c_str())) return true;
    else return false;
}

bool MainWindow::update_available(QString pkgname, QString new_version)
{
    alpm_db_t *localdb = alpm_get_localdb(config.getConfigHandle());
    alpm_pkg_t *pkg = alpm_db_get_pkg(localdb,pkgname.toStdString().c_str());
    QString old_version = alpm_pkg_get_version(pkg);
    if(common->compare_version(old_version,new_version)) return true;
    return false;
}

void MainWindow::refresh_installed_package_list()
{
    QList<pkg_display_t> list = alpmhelper.list_installed_packages(&config,get_flag());
    updates = new UpdateDialog(&config, list);
    int i = 0;
    package_list_widget->setRowCount(list.size());

    foreach (pkg_display_t pkg, list) {
        QTableWidgetItem *icon_item = new QTableWidgetItem;
        QPixmap p;
        if(!p.load(":/icons/installed")) QTextStream(stdout) << "Error" << endl;
        icon_item->setData(Qt::DecorationRole,p.scaled(36,36));
        package_list_widget->setItem(i,0,icon_item);
        package_list_widget->setItem(i,1,new QTableWidgetItem(pkg.name));
        package_list_widget->setItem(i,2,new QTableWidgetItem(pkg.version));
        package_list_widget->setItem(i,3,new QTableWidgetItem(pkg.repo));
        package_list_widget->setRowHeight(i,50);
        i++;
    }
}

void MainWindow::display_aur_package_list(QList<aurpkg_t> list)
{
    int i = 0;
    package_list_widget->setRowCount(0);
    package_list_widget->setRowCount(list.size());
    foreach (aurpkg_t pkg, list) {
        QTableWidgetItem *icon_item = new QTableWidgetItem;
        QPixmap p;
        if(is_installed(pkg.name)){
            if(update_available(pkg.name, pkg.version)){
                if(!p.load(":/icons/update")) QTextStream(stdout) << "Error" << endl;
            }else if(!p.load(":/icons/installed")) QTextStream(stdout) << "Error" << endl;
        }else{
            if(!p.load(":/icons/package")) QTextStream(stdout) << "Error" << endl;
        }
        icon_item->setData(Qt::DecorationRole,p.scaled(36,36));
        package_list_widget->setItem(i,0,icon_item);
        package_list_widget->setItem(i,1,new QTableWidgetItem(pkg.name));
        package_list_widget->setItem(i,2,new QTableWidgetItem(pkg.version));
        package_list_widget->setItem(i,3,new QTableWidgetItem("AUR"));
        package_list_widget->setRowHeight(i,50);
        i++;
    }
}

void MainWindow::display_repo_package_list(QList<pkg_display_t> list)
{
    int i = 0;
    package_list_widget->setRowCount(0);
    package_list_widget->setRowCount(list.size());
    foreach (pkg_display_t pkg, list) {
        QTableWidgetItem *icon_item = new QTableWidgetItem;
        QPixmap p;
        if(pkg.isInstalled){
            if(update_available(pkg.name, pkg.version)){
                if(!p.load(":/icons/update")) QTextStream(stdout) << "Error" << endl;
            }else if(!p.load(":/icons/installed")) QTextStream(stdout) << "Error" << endl;
        }else{
            if(!p.load(":/icons/package")) QTextStream(stdout) << "Error" << endl;
        }
        icon_item->setData(Qt::DecorationRole,p.scaled(36,36));
        package_list_widget->setItem(i,0,icon_item);
        package_list_widget->setItem(i,1,new QTableWidgetItem(pkg.name));
        package_list_widget->setItem(i,2,new QTableWidgetItem(pkg.version));
        package_list_widget->setItem(i,3,new QTableWidgetItem(pkg.repo));
        package_list_widget->setRowHeight(i,50);
        i++;
    }
}

void MainWindow::display_packages_list(QList<aurpkg_t> list)
{
    foreach (aurpkg_t pkg, list) {
        pkg_display_t t;
        t.name = pkg.name;
        t.version = pkg.version;
        t.desc = pkg.desc;
        t.isInstalled = is_installed(pkg.name);
        t.repo = "AUR";
        total_list.append(t);
    }

    int i = 0;

    package_list_widget->setRowCount(0);
    package_list_widget->setRowCount(total_list.size());

    foreach (pkg_display_t pkg, total_list) {
        QTableWidgetItem *icon_item = new QTableWidgetItem;
        QPixmap p;
        if(pkg.isInstalled){
            if(update_available(pkg.name, pkg.version)){
                if(!p.load(":/icons/update")) QTextStream(stdout) << "Error" << endl;
            }else if(!p.load(":/icons/installed")) QTextStream(stdout) << "Error" << endl;
        }else{
            if(!p.load(":/icons/package")) QTextStream(stdout) << "Error" << endl;
        }
        icon_item->setData(Qt::DecorationRole,p.scaled(36,36));
        package_list_widget->setItem(i,0,icon_item);
        package_list_widget->setItem(i,1,new QTableWidgetItem(pkg.name));
        package_list_widget->setItem(i,2,new QTableWidgetItem(pkg.version));
        package_list_widget->setItem(i,3,new QTableWidgetItem(pkg.repo));
        package_list_widget->setRowHeight(i,50);
        i++;
    }
    total_list.clear();
    disconnect(&aurhelper,SIGNAL(downloadfinished(QList<aurpkg_t>)),this,0);
}

void MainWindow::search_packages()
{
    QString search_text = search_bar->text();
    int flag = get_flag();

    if(flag == ALPMHelper::FOREIGN_PACKAGES){
        aurhelper.search_packages(search_text);
        connect(&aurhelper,SIGNAL(downloadfinished(QList<aurpkg_t>)),this,SLOT(display_aur_package_list(QList<aurpkg_t>)));
    }else if(flag == ALPMHelper::REPO_PACKAGES){
        display_repo_package_list(alpmhelper.search_packages(&config,search_text));
    }else if(flag == ALPMHelper::ALL_PACKAGES){
        total_list.append(alpmhelper.search_packages(&config,search_text));
        aurhelper.search_packages(search_text);
        connect(&aurhelper,SIGNAL(downloadfinished(QList<aurpkg_t>)),this,SLOT(display_packages_list(QList<aurpkg_t>)));
    }
}

void MainWindow::set_connections()
{
    connect(quit_action,SIGNAL(triggered(bool)),this,SLOT(close()));
    connect(settings_action,SIGNAL(triggered(bool)),settings,SLOT(show()));
    connect(refresh_button,SIGNAL(clicked(bool)),this,SLOT(refresh_installed_package_list()));
    connect(search_button,SIGNAL(clicked(bool)),this,SLOT(search_packages()));
    connect(search_bar,SIGNAL(returnPressed()),this,SLOT(search_packages()));
    connect(update_button,SIGNAL(clicked(bool)),updates,SLOT(show()));
}

void MainWindow::create_display()
{
    main_layout = ui->mainLayout;

    QPixmap p;
    if(!p.load(":/icons/app-icon")) QTextStream(stdout) << "Error" << endl;
    QIcon app_icon = QIcon(p);
    setWindowIcon(app_icon);
    setWindowTitle("Pac-Assist");

    quit_action = ui->actionQuit;
    settings_action = ui->actionSettings;
    aur_option = ui->actionAUR;
    repo_option = ui->actionOfficial;

    settings = new SettingsDialog(config);
    search_layout = new QHBoxLayout;

    refresh_button = new QPushButton("Refresh Pkg List");
    search_button = new QPushButton("Search");
    update_button = new QPushButton("Check for Update");

    search_bar = new QLineEdit;
    search_bar->setPlaceholderText("Search Package...");

    search_layout->addWidget(refresh_button);
    search_layout->addWidget(search_bar);
    search_layout->addWidget(search_button);
    search_layout->addWidget(update_button);

    package_list_widget = new QTableWidget;
    set_column_width(package_list_widget->width());
    QStringList labels;
    labels << "Status" << "Name" << "Version" << "Repository";
    package_list_widget->setColumnCount(4);
    package_list_widget->setHorizontalHeaderLabels(labels);
    refresh_installed_package_list();

    main_layout->addLayout(search_layout);
    main_layout->addWidget(package_list_widget);

    set_connections();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    config()
{
    ui->setupUi(this);
    create_display();
}

MainWindow::~MainWindow()
{
    delete ui;
}
