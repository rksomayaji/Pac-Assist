#-------------------------------------------------
#
# Project created by QtCreator 2017-11-11T11:06:53
#
#-------------------------------------------------

QT       += core gui network KAuth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Pac-assist
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    config.cpp \
    helper/githelper.cpp \
    helper/alpmhelper.cpp \
    helper/aurhelper.cpp \
    helper/kauthhelper.cpp \
    util/common.cpp \
    dialogs/settingsdialog.cpp \
    dialogs/updatedialog.cpp

HEADERS += \
        mainwindow.h \
    config.h \
    helper/githelper.h \
    helper/alpmhelper.h \
    helper/aurhelper.h \
    dialogs/settingsdialog.h \
    helper/kauthhelper.h \
    util/types.h \
    util/common.h \
    dialogs/updatedialog.h

FORMS += \
        mainwindow.ui

unix:!macx: LIBS += -lalpm

unix:!macx: LIBS += -lgit2

DISTFILES += \
    CMakeLists.txt \
    com.pacassisst.privileged.actions \
    icons/arch_pkg_download.png \
    icons/arch_pkg_install.png \
    icons/arch_pkg_uninstall_red.png \
    icons/arch_pkg_update.png \
    icons/pac-assist-flower.png \
    icons/arch-sh-600x600-300x300.png \
    icons/arch_pkg.png \
    icons/arch_pkg_installed.png

RESOURCES += \
    mainwindow.qrc
