#include "config.h"

QString trim_string(QString line){
    QString newString;

    for (int i = 1; i < line.length()-1; i++){
        if(line.at(i)=="[") continue;
        newString.append(line.at(i));
    }
    return newString;
}

repo_t parse_add_server(repo_t repo, QString arch){
    if(repo.isActive){
        if(repo.include_serverlist != NULL){
            QFile mirrorlist(repo.include_serverlist);
            QTextStream in(&mirrorlist);
            QString line;

            if(mirrorlist.open(QIODevice::ReadOnly)){
                while(!in.atEnd()){
                    line = in.readLine();
                    if((line.at(0) == "#") == 1)
                    {
                        continue;
                    }
                    QString key = line.split(" = ")[0];
                    QString server = line.split(" = ")[1];
                    server.replace("$repo", repo.name, Qt::CaseInsensitive);
                    server.replace("$arch", arch, Qt::CaseInsensitive);
                    repo.server.append(server);
                }
            }
        }
    }
    return repo;
}

int parse_sig_level(QString level)
{
    if(level.compare("default")) return ALPM_SIG_USE_DEFAULT;
    else return ALPM_SIG_USE_DEFAULT;
}

QStringList ignore_list(QString value)
{
    QStringList return_list;

    foreach (QString v, value.split(" ")) {
        if(v != ""){
            return_list.append(v);
        }
    }

    return return_list;
}

void Config::set_arch(QString arch){
    if(arch.compare(" auto") == 0){
        struct utsname unixName;
        uname(&unixName);
        config.arch = unixName.machine;
    }
    else{
        config.arch = arch;
    }
}

void Config::parse_options(QString key, QString value){
    if(key.contains("RootDir")){
        if(key.at(0) == "#") config.rootdir = "/";
        else config.rootdir = value;
    }
    if(key.contains("DBPath")){
        if(key.at(0) == "#") config.dbpath = "/var/lib/pacman";
        else config.dbpath = value;
    }
    if(key.contains("CacheDir")){
        if(key.at(0) == "#") config.cachedir = "/var/cache/pacman/pkg/";
        else config.cachedir = value;
    }
    if(key.contains("LogFile")){
        if(key.at(0) == "#") config.logfile = "/var/log/pacman.log";
        else config.logfile = value;
    }
    if(key.contains("GPGDir")){
        if(key.at(0) == "#") config.gpgdir = "/etc/pacman.d/gnupg/";
        else config.dbpath = value;
    }
    if(key.contains("HookDir")){
        if(key.at(0) == "#") config.hookdir = "/etc/pacman.d/hooks/";
        else config.dbpath = value;
    }
    if(key.contains("IgnorePkg")){
        if(key.at(0) != "#") config.ignorepkg = ignore_list(value);
    }
    if(key.contains("IgnoreGroup")){
        if(key.at(0) != "#") config.ignoregrp = ignore_list(value);
    }
    if(key.contains("NoUpgrade")){
        if(key.at(0) != "#") config.noupgrade = ignore_list(value);
    }
    if(key.contains("NoExtract")){
        if(key.at(0) != "#") config.noextract = ignore_list(value);
    }
    if(key.contains("HoldPkg")){
        if(key.at(0) != "#") config.holdpkg = ignore_list(value);
    }
    if(key.contains("RemoteFileSigLevel")){
        if(key.at(0) == "#") config.dbpath = "/var/lib/pacman";
        else config.dbpath = value;
    }
    if(key.contains("SigLevel")){
        if(key.at(0) == "#") config.siglevel = parse_sig_level("default");
        else config.siglevel = parse_sig_level(value);
    }
    if(key.contains("LocalFileSigLevel")){
        if(key.at(0) == "#") config.localfilesiglevel = parse_sig_level("default");
        else config.localfilesiglevel = parse_sig_level(value);
    }
    if(key.contains("RemoteFileSigLevel")){
        if(key.at(0) == "#") config.remotefilesiglevel = parse_sig_level("default");
        else config.remotefilesiglevel = parse_sig_level(value);
    }
    if(key.compare("Architecture ") == 0){
        set_arch(value);
    }
}

void Config::parse_config(){
    QFile configfile(config.configfile);
    QString line;
    QTextStream filestream(&configfile);

    if(configfile.open(QIODevice::ReadOnly)){
        while(!filestream.atEnd()){
            line = filestream.readLine();
            QString section;
            int len = line.length();

            if((len > 1) == 1){
                if(line.front() == "["){
                    section = trim_string(line);

                    if(section == "options"){
                        bool is_options = true;
                        while(is_options){
                            line = filestream.readLine(80);
                            len = line.length();

                            if((len > 1) == 1){
                                if(line.contains("=")){
                                    QString key = line.split("=")[0];
                                    QString value = line.split("=")[1];
                                    parse_options(key,value);
                                }
                            }
                            if((len > 1) == 1 && line.contains("REPOSITORIES")) is_options = false;
                        }
                    }else{
                        repo_t repo;
                        repo.name = section;
                        repo.isActive = true;

                        QString line_next = line;
                        while(line_next.length() != 0){
                            line_next = filestream.readLine();
                            if (line_next.length() != 0){
                                QString key = line_next.split(" = ")[0];
                                QString value = line_next.split(" = ")[1];

                                if(key.compare("Include") == 0){
                                    repo.include_serverlist = value;
                                    repo = parse_add_server(repo, config.arch);
                                }else if(key.compare("Server") == 0){
                                    repo.server.append(value);
                                }else if(key.compare("SigLevel") == 0){
                                    repo.siglevel = 0;
                                }
                            }
                        }
                        config.repos.append(repo);
                    }
                }else if(line.at(0) == "#" && line.at(1) == "["){
                    section = trim_string(line);
                    if(section != "custom"){
                        repo_t repo;
                        repo.name = section;
                        config.repos.append(repo);
                    }
                }
            }
        }
    }else{
        QTextStream(stdout) << configfile.errorString() << endl;
    }
}

alpm_list_t *packages_list(QStringList pkgs)
{
    alpm_list_t *retpkgs = NULL;
    foreach (QString p, pkgs) {
        char *pkg = (char*)p.toStdString().c_str();
        QTextStream(stdout) << pkg << endl;
        retpkgs = alpm_list_add(retpkgs,pkg);
    }
    return retpkgs;
}

alpm_handle_t *Config::init_alpm(QString rootpath, QString dbspath){
    alpm_handle_t *newHandle = NULL;
    alpm_errno_t err;

    const char* root = rootpath.toStdString().c_str();
    const char* dbpath = dbspath.toStdString().c_str();

    newHandle = alpm_initialize(root, dbpath, &err);

    if (newHandle == NULL){
        QTextStream(stdout) << "Cannot initialize libALPM. " << alpm_strerror(err) << endl;
        if(err == ALPM_ERR_DB_VERSION) {
            QTextStream(stdout) << "Try running pacman-db-upgrade." << endl;
        }
        return NULL;
    }

    alpm_option_set_arch(newHandle,config.arch.toStdString().c_str());
    alpm_option_set_logfile(newHandle,config.logfile.toStdString().c_str());
    alpm_option_set_gpgdir(newHandle,config.gpgdir.toStdString().c_str());
    alpm_option_add_cachedir(newHandle,config.cachedir.toStdString().c_str());

    alpm_option_set_ignorepkgs(newHandle,packages_list(config.ignorepkg));
    alpm_option_set_ignoregroups(newHandle,packages_list(config.ignoregrp));

    alpm_option_set_default_siglevel(newHandle,ALPM_SIG_USE_DEFAULT);
    alpm_option_set_local_file_siglevel(newHandle,ALPM_SIG_USE_DEFAULT);
    alpm_option_set_remote_file_siglevel(newHandle,ALPM_SIG_USE_DEFAULT);

    alpm_option_set_noupgrades(newHandle,packages_list(config.noupgrade));
    alpm_option_set_noextracts(newHandle,packages_list(config.noextract));

    return newHandle;
}

void register_syncdbs(QList<repo_t> repos, alpm_handle_t *handle){
    foreach (repo_t repo, repos) {
        if(repo.isActive){
            alpm_db_t *db = alpm_register_syncdb(handle, repo.name.toStdString().c_str(), ALPM_SIG_USE_DEFAULT);
            if(db == NULL){
                QTextStream(stdout) << "Cannot register " << repo.name << " database. " << alpm_strerror(alpm_errno(handle)) << endl;
            }else{
                foreach (QString url, repo.server) {
                    alpm_db_add_server(db, url.toStdString().c_str());
                }
            }
        }
    }
}

Config::Config()
{
    config.configfile = "/etc/pacman.conf";
    parse_config();

    config.handle = init_alpm(config.rootdir, config.dbpath);
    
    register_syncdbs(config.repos, config.handle);
}

Config *Config::operator = (const Config *c)
{
    config = c->config;
    return this;
}
