#ifndef CONFIG_H
#define CONFIG_H

#include <alpm.h>
#include <alpm_list.h>
#include <sys/utsname.h>

#include <QList>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QFile>

#include "util/types.h"

class Config
{
public:
    Config();

    Config* operator = (const Config *c);

    alpm_handle_t *getConfigHandle(){
        return config.handle;
    }
    QString getArchitecture(){
        return config.arch;
    }
    QString getRootDir(){
        return config.rootdir;
    }
    QString getConfigFilePath(){
        return config.configfile;
    }
    QString getdbPath(){
        return config.dbpath;
    }
    QString getCacheDir(){
        return config.cachedir;
    }
    QString getGPGDir(){
        return config.gpgdir;
    }
    QString getLogFilePath(){
        return config.logfile;
    }
    QString getHookDir(){
        return config.hookdir;
    }
    QStringList getHoldPkgList(){
        return config.holdpkg;
    }
    QStringList getIgnorePkgList(){
        return config.ignorepkg;
    }
    QStringList getNoUpgradePkgList(){
        return config.noupgrade;
    }
    QStringList getNoExtractFileList(){
        return config.noextract;
    }
    QStringList getIgnoreGroupList(){
        return config.ignoregrp;
    }
    QList<repo_t> getConfigRepositories(){
        return config.repos;
    }

    void set_arch(QString arch);

    Config *new_config()
    {
        return this;
    }

private:
    config_t config;
    void parse_config();
    void parse_options(QString key, QString value);
    alpm_handle_t *init_alpm(QString rootpath, QString dbspath);
};

#endif // CONFIG_H
