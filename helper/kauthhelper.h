#ifndef KAUTHHELPER_H
#define KAUTHHELPER_H

#include <QDebug>

#include <KAuth>

using namespace KAuth;

class kauthhelper : public QObject
{
    Q_OBJECT
public:
    kauthhelper();

public Q_SLOTS:
    ActionReply install_packages(const QVariantMap& args)
    {
        qDebug() << "Inside the helper" << endl;
    }
};

//#include "kauthhelper.moc"

#endif // KAUTHHELPER_H
