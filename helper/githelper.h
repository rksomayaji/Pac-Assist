#ifndef GITHELPER_H
#define GITHELPER_H

#include <git2.h>
#include <git2/clone.h>

#include <QString>
#include <QProgressBar>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDialog>
#include <QDir>
#include <QTextStream>

#include "../util/types.h"

class GitProgressDialog : public QDialog
{
public:
    GitProgressDialog(QWidget *parent = 0);
    void setProgress(progress_data pd);
private:
    QVBoxLayout *main_layout;
    QHBoxLayout *button_layout;

    QLabel *pkgname_label;
    QLabel *filename_label;

    QProgressBar *pkg_dnld_progressbar;
    QProgressBar *file_dnld_progressbar;

    QPushButton *stop_button;

};

class GitHelper
{
public:
    GitHelper();
    void clone_snapshot(QString pkgName);
private:
    GitProgressDialog *dnld_dialog;
};

#endif // GITHELPER_H
