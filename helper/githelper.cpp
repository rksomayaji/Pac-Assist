#include "githelper.h"

GitHelper::GitHelper()
{
    git_libgit2_init();
}

static void display_progress(const progress_data *pd){
    QTextStream(stdout) << pd->path << endl;
}

static int sideband_progress(const char *str, int len, void *payload){
    (void)payload; // unused
    return 0;
}

static int fetch_progress(const git_transfer_progress *stats, void *payload){
  progress_data *pd = (progress_data*)payload;
  pd->fetch_progress = *stats;
  display_progress(pd);
  return 0;
}

static void checkout_progress(const char *path, size_t cur, size_t tot, void *payload){
  progress_data *pd = (progress_data*)payload;
  pd->completed_steps = cur;
  pd->total_steps = tot;
  pd->path = path;
  display_progress(pd);
}

void GitHelper::clone_snapshot(QString pkgName){
    progress_data pd ={{0}};
    git_repository *cloned_repo = NULL;
    git_clone_options clone_options = GIT_CLONE_OPTIONS_INIT;
    git_checkout_options chkout_options = GIT_CHECKOUT_OPTIONS_INIT;
    QString url = "https://aur.archlinux.org/" + pkgName + ".git";
    QString path = QDir::homePath() + "/.local/share/Pac-assist/" + pkgName;
    int error;

    chkout_options.checkout_strategy = GIT_CHECKOUT_SAFE;
    chkout_options.progress_cb = checkout_progress;
    chkout_options.progress_payload = &pd;
    clone_options.checkout_opts = chkout_options;
    clone_options.fetch_opts.callbacks.sideband_progress = sideband_progress;
    clone_options.fetch_opts.callbacks.transfer_progress = &fetch_progress;
    clone_options.fetch_opts.callbacks.payload = &pd;

    error = git_clone(&cloned_repo, url.toStdString().c_str(), path.toStdString().c_str(), &clone_options);
    if(error != 0){
        const git_error *err = giterr_last();
        QTextStream(stdout) << err->message << endl;
    }else if (cloned_repo) {
        git_repository_free(cloned_repo);
    }
}
