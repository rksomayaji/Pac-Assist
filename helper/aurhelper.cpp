#include "aurhelper.h"

AURHelper::AURHelper(QObject *parent) : QObject(parent)
{

}

aurpkg_t AURHelper::parse_aur_package(const QJsonValue *package)
{
    aurpkg_t aurpkg;

    aurpkg.name = package->toObject().value("Name").toString();
    aurpkg.desc = package->toObject().value("Description").toString();
    aurpkg.version = package->toObject().value("Version").toString();
    aurpkg.maintainer = package->toObject().value("Maintainer").toString();
    aurpkg.url = package->toObject().value("URL").toString();
    aurpkg.urlpath = package->toObject().value("URLPath").toString();

    aurpkg.pkgbase = package->toObject().value("PackageBase").toString();

    QJsonArray depend = package->toObject().value("Depends").toArray();

    foreach(const QJsonValue &q, depend){
        QString f = q.toString();
        aurpkg.depends.append(f);
    }

    QJsonArray conflict = package->toObject().value("Conflicts").toArray();

    foreach(const QJsonValue &q, conflict){
        QString f = q.toString();
        aurpkg.conflicts.append(f);
    }

    QJsonArray makedepend = package->toObject().value("MakeDepends").toArray();

    foreach(const QJsonValue &q, makedepend){
        QString f = q.toString();
        aurpkg.makedepends.append(f);
    }

    QJsonArray optdepend = package->toObject().value("OptDepends").toArray();

    foreach(const QJsonValue &q, optdepend){
        QString f = q.toString();
        aurpkg.optdepends.append(f);
    }

    QJsonArray license = package->toObject().value("License").toArray();

    foreach(const QJsonValue &q, license){
        QString f = q.toString();
        aurpkg.licenses.append(f);
    }

    QJsonArray group = package->toObject().value("Groups").toArray();

    foreach(const QJsonValue &q, group){
        QString f = q.toString();
        aurpkg.groups.append(f);
    }

    QJsonArray keyword = package->toObject().value("Keywords").toArray();

    foreach(const QJsonValue &q, keyword){
        QString f = q.toString();
        aurpkg.keywords.append(f);
    }

    QJsonArray provide = package->toObject().value("Provides").toArray();

    foreach(const QJsonValue &q, provide){
        QString f = q.toString();
        aurpkg.provides.append(f);
    }

    QJsonArray replace = package->toObject().value("Replaces").toArray();

    foreach(const QJsonValue &q, replace){
        QString f = q.toString();
        aurpkg.replaces.append(f);
    }

    aurpkg.id = package->toObject().value("ID").toInt(0);
    aurpkg.pkgbase_id = package->toObject().value("PackageBaseID").toInt(0);
    aurpkg.votes = package->toObject().value("Votes").toInt(0);

    aurpkg.outofdate = package->toObject().value("OutOfDate").toBool(false);

    aurpkg.firstsubmit = (time_t) package->toObject().value("FirstSubmitted").toInt(0);
    aurpkg.lastmod = (time_t) package->toObject().value("LastModified").toInt(0);

    aurpkg.popularity = package->toObject().value("Popularity").toDouble(0);

    return aurpkg;
}

void AURHelper::get_packages(QNetworkReply *reply)
{
    aurpkg_t package;
    QList<aurpkg_t> packages;

    if(reply->error()){
        QTextStream(stdout) << reply->errorString();
        reply->deleteLater();
    }else{
        QByteArray *json_reply = new QByteArray();
        json_reply->append(reply->readAll());

        QJsonDocument list = QJsonDocument::fromJson(*json_reply);

        if (!list.isObject()){
            QTextStream(stdout) << "JSON is not an object";
        }

        QJsonObject jObject=list.object();
        if (jObject.isEmpty()){QTextStream(stdout) << "JSON object is empty";}

        QString resultType = jObject.value("type").toString();
        if(resultType == "error"){
            QTextStream(stdout) << jObject.value("error").toString();
        }
        else if(resultType == "search"){
            QJsonValue arrayValue = jObject.value("results");
            QJsonArray array = arrayValue.toArray();

            foreach(const QJsonValue &v, array){
                package = parse_aur_package(&v);
                packages.append(package);
            }
            emit downloadfinished(packages);
        }else if(resultType == "multiinfo"){
            if(flag == VERSION){
                QJsonValue arrayValue = jObject.value("results");
                QJsonArray array = arrayValue.toArray();
                QList<pkg_display_t> updates;
                int i = 0;

                foreach(const QJsonValue &v, array){
                    package = parse_aur_package(&v);
                    foreach (pkg_display_t p, versions) {
                        if(p.name == package.name){
                            if(common.compare_version(p.version,package.version))
                            {
                                pkg_display_t pkg;
                                QTextStream(stdout) << package.name << endl;
                                pkg.name = package.name;
                                pkg.new_version = package.version;
                                pkg.version = p.version;
                                pkg.updateAvailable = true;
                                updates.append(pkg);
                            }
                        }
                    }
                    i++;
                }
                emit(updatesavilablefor(updates));
            }
        }
        reply->deleteLater();
    }
}

void AURHelper::search_packages(QString search_text)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager();

    QUrl url("http://aur.archlinux.org/rpc/?");

    QUrlQuery query;
    query.addQueryItem("v","5");
    query.addQueryItem("type","search");
    query.addQueryItem("by","name-desc");
    query.addQueryItem("arg",search_text);

    url.setQuery(query);

    QNetworkRequest *request = new QNetworkRequest(url);
    request->setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    manager->get(*request);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(get_packages(QNetworkReply*)));
}

void AURHelper::check_updates(QList<pkg_display_t> list)
{
    versions = list;
    QNetworkAccessManager *manager = new QNetworkAccessManager();

    QUrl url("http://aur.archlinux.org/rpc/?");

    QUrlQuery query;
    query.addQueryItem("v","5");
    query.addQueryItem("type","info");
    query.addQueryItem("by","name-desc");

    foreach (pkg_display_t pkg, list) {
        query.addQueryItem("arg[]",pkg.name);
    }

    url.setQuery(query);

    flag = VERSION;
    QNetworkRequest *request = new QNetworkRequest(url);
    request->setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    manager->get(*request);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(get_packages(QNetworkReply*)));
}
