#ifndef AURHELPER_H
#define AURHELPER_H

#include <QObject>
#include <QUrl>
#include <QUrlQuery>
#include <QByteArray>
#include <QJsonArray>
#include <QJsonValue>
#include <QStringList>
#include <QTextStream>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>

#include <alpm.h>
#include <alpm_list.h>

#include "../util/types.h"
#include "../util/common.h"

class AURHelper : public QObject
{
    Q_OBJECT
public:
    explicit AURHelper(QObject *parent = nullptr);
    void search_packages(QString search_text);
    void check_updates(QList<pkg_display_t> list);

    enum flags{
        VERSION,
        INFO
    };
signals:
    void downloadfinished(QList<aurpkg_t> packages);
    void updatesavilablefor(QList<pkg_display_t> list);
public slots:
    void get_packages(QNetworkReply *reply);
private:
    aurpkg_t parse_aur_package(const QJsonValue *package);
    int flag;
    QList<pkg_display_t> versions;
    Common common;
};

#endif // AURHELPER_H
