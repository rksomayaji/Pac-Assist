#ifndef ALPMHELPER_H
#define ALPMHELPER_H

#include <QObject>
#include <QTextStream>

#include <alpm.h>
#include <alpm_list.h>

#include "../config.h"
#include "../util/types.h"
#include "../util/common.h"

class ALPMHelper : public QObject
{
    Q_OBJECT
public:
    explicit ALPMHelper(QObject *parent = nullptr);
    QList<pkg_display_t> list_installed_packages(Config *config, int flags);
    QList<pkg_display_t> search_packages(Config *config, QString search_text);
    QList<pkg_display_t> check_updates(Config *config, QList<pkg_display_t> list);

    enum Flags{
        FOREIGN_PACKAGES,
        REPO_PACKAGES,
        ALL_PACKAGES
    };

signals:

public slots:

private:
    Common common;
};

#endif // ALPMHELPER_H
