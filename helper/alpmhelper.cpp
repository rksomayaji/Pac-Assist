#include "alpmhelper.h"

ALPMHelper::ALPMHelper(QObject *parent) : QObject(parent)
{

}

QList<pkg_display_t> ALPMHelper::list_installed_packages(Config *config, int flags)
{
    QList<pkg_display_t> pkgs;
    pkg_display_t pkg_display;
    pkg_display.isInstalled = true;
    alpm_db_t *local_db = alpm_get_localdb(config->getConfigHandle());
    alpm_list_t *local_cache = alpm_db_get_pkgcache(local_db);
    alpm_list_t *sync_dbs = alpm_get_syncdbs(config->getConfigHandle());
    bool local = false;

    for(alpm_list_t *i = local_cache; i; i = alpm_list_next(i))
    {
        alpm_pkg_t *pkg = (alpm_pkg_t*)i->data;
        if(alpm_pkg_get_reason(pkg) == ALPM_PKG_REASON_EXPLICIT)
        {
            for(alpm_list_t *j = sync_dbs; j; j = alpm_list_next(j))
            {
                alpm_db_t *db = (alpm_db_t*)j->data;
                alpm_pkg_t *db_pkg = alpm_db_get_pkg(db,alpm_pkg_get_name(pkg));
                if(db_pkg != NULL) {
                    local = true;
                    pkg_display.repo = alpm_db_get_name(db);
                    break;
                }else local = false;
            }
            pkg_display.name = alpm_pkg_get_name(pkg);
            pkg_display.desc = alpm_pkg_get_desc(pkg);
            pkg_display.version = alpm_pkg_get_version(pkg);
            if(!local) {
                pkg_display.repo = "AUR";
                if(flags == FOREIGN_PACKAGES || flags == ALL_PACKAGES) pkgs.append(pkg_display);
            }else {
                if(flags == REPO_PACKAGES || flags == ALL_PACKAGES) pkgs.append(pkg_display);
            }
        }
    }
    return pkgs;
}

QList<pkg_display_t> ALPMHelper::search_packages(Config *config, QString search_text)
{
    QList<pkg_display_t> result;
    alpm_list_t *search_result, *dbs;
    alpm_list_t *needles = NULL;
    pkg_display_t pkg_display;
    alpm_db_t *localdb;
    char *search_key = (char*) search_text.toStdString().c_str();

    needles = alpm_list_add(needles, search_key);
    dbs = alpm_get_syncdbs(config->getConfigHandle());
    localdb = alpm_get_localdb(config->getConfigHandle());

    for(alpm_list_t *i = dbs; i; i=alpm_list_next(i)){
        alpm_db_t *db = (alpm_db_t*) i->data;
        search_result = alpm_db_search(db,needles);
        if(search_result != NULL)
        {
            for(alpm_list_t *j = search_result; j; j = alpm_list_next(j))
            {
                alpm_pkg_t *pkg = (alpm_pkg_t*) j->data;

                pkg_display.repo = alpm_db_get_name(db);
                pkg_display.name = alpm_pkg_get_name(pkg);
                pkg_display.desc = alpm_pkg_get_desc(pkg);
                pkg_display.version = alpm_pkg_get_version(pkg);

                if(alpm_db_get_pkg(localdb,alpm_pkg_get_name(pkg))) pkg_display.isInstalled = true;
                else pkg_display.isInstalled = false;

                result.append(pkg_display);
            }
        }
    }
    return result;
}

QList<pkg_display_t> ALPMHelper::check_updates(Config *config, QList<pkg_display_t> list)
{
    alpm_list_t *sync_dbs = alpm_get_syncdbs(config->getConfigHandle());
    alpm_list_t *local_cache = alpm_db_get_pkgcache(alpm_get_localdb(config->getConfigHandle()));
    QList<pkg_display_t> ret;

    for(alpm_list_t *c = local_cache; c; c = alpm_list_next(c))
    {
        alpm_pkg_t *cp = (alpm_pkg_t*) c->data;
        for(alpm_list_t *i = sync_dbs; i; i = alpm_list_next(i))
        {
            alpm_db_t *db = (alpm_db_t*)i->data;
            alpm_pkg_t *p = alpm_db_get_pkg(db,alpm_pkg_get_name(cp));

            if(p != NULL)
            {
                if(common.compare_version(alpm_pkg_get_version(cp),alpm_pkg_get_version(p)))
                {
                    pkg_display_t pkg;
                    pkg.name = alpm_pkg_get_name(cp);
                    pkg.version = alpm_pkg_get_version(cp);
                    pkg.new_version = alpm_pkg_get_version(p);
                    pkg.updateAvailable = true;
                    QTextStream(stdout) << alpm_pkg_get_name(p) << endl;
                    ret.append(pkg);
                }
            }
        }
    }

    return ret;
}
